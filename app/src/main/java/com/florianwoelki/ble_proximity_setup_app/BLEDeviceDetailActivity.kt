package com.florianwoelki.ble_proximity_setup_app

import android.bluetooth.le.ScanResult
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.florianwoelki.ble_proximity_setup_app.api.IProxim
import com.florianwoelki.ble_proximity_setup_app.api.Proxim
import com.florianwoelki.ble_proximity_setup_app.api.beacon.model.BeaconModel
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.device_detail_row.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.properties.Delegates

class BLEDeviceDetailActivity : AppCompatActivity() {

    private var longitude: Double by Delegates.notNull()
    private var latitude: Double by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val navBarTitle = intent.getStringExtra(MainViewHolder.DEVICE_TITLE_KEY)
        supportActionBar?.title = navBarTitle

        val beaconInfo = intent.getSerializableExtra(MainViewHolder.BEACON_INFO_KEY) as BeaconModel
        val secondBeaconInfo = intent.getSerializableExtra(MainViewHolder.SECOND_BEACON_INFO_KEY) as BeaconModel
        val thirdBeaconInfo = intent.getSerializableExtra(MainViewHolder.THIRD_BEACON_INFO_KEY) as BeaconModel
        val fourthBeaconInfo = intent.getSerializableExtra(MainViewHolder.FOURTH_BEACON_INFO_KEY) as BeaconModel

        latitude = intent.getDoubleExtra(MainViewHolder.GPS_LATITUDE, 0.0)
        longitude = intent.getDoubleExtra(MainViewHolder.GPS_LONGITUDE, 0.0)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = DeviceDetailAdapter(beaconInfo, secondBeaconInfo, thirdBeaconInfo, fourthBeaconInfo, latitude, longitude)
    }

    private class DeviceDetailAdapter(val beacon: BeaconModel, val secondBeacon: BeaconModel? = null, val thirdBeacon: BeaconModel? = null, val fourthBeacon: BeaconModel? = null, val latitude: Double, val longitude: Double) : RecyclerView.Adapter<DeviceDetailViewHolder>(),
        IProxim {

        private var startedRecordingData = false
        private var recordedData: MutableList<HashMap<String, Any>> = mutableListOf()
        private var recordedData2: MutableList<HashMap<String, Any>> = mutableListOf()
        private var recordedData3: MutableList<HashMap<String, Any>> = mutableListOf()
        private var recordedData4: MutableList<HashMap<String, Any>> = mutableListOf()

        private var viewHolder by Delegates.notNull<DeviceDetailViewHolder>()

        override fun getItemCount(): Int {
            return 1
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceDetailViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val customView = layoutInflater.inflate(R.layout.device_detail_row, parent, false)
            return DeviceDetailViewHolder(customView)
        }

        override fun onBindViewHolder(holder: DeviceDetailViewHolder, position: Int) {
            viewHolder = holder

            val constructedInformationText = "Timestamp: ${Date().time}\n" +
                    "Owner: ${Build.MODEL}\n" +
                    "GPS: (lat: $latitude, lng: $longitude)\n\n" +
                    "Company: ${beacon.packet.company}\n" +
                    "Name: ${beacon.name}\n" +
                    "TX: ${beacon.txPowerLevel}\n" +
                    "RSSI: ${beacon.rssi}\n" +
                    "AdvertisingSid: ${beacon.advertisingSid}\n" +
                    "Manufacturer Data: \n${beacon.packet.manufacturerData}\n" +
                    "Packet info: (Length: ${beacon.packet.length}, Flags: ${beacon.packet.flags}, Raw: ${beacon.packet.rawPacketData})\n"

            holder.view.informationTextView.text = constructedInformationText


            holder.view.record_data.setOnClickListener {
                startedRecordingData = !startedRecordingData
                holder.view.record_data.text = if (startedRecordingData) "Stop record Data" else "Start record data"

                if (startedRecordingData) {
                    // Clear array
                    recordedData = mutableListOf()
                    recordedData2 = mutableListOf()
                    recordedData3 = mutableListOf()
                    recordedData4 = mutableListOf()

                    // Start scanning
                    api = Proxim(holder.view.context, this)
                    api.startScanning()
                } else {
                    api.stopScanning()


                    insertData(holder.view.editPrefixFirebaseField)
                }
            }
        }

        private fun insertData(textField: EditText) {
            val name = textField.text
            val db = FirebaseFirestore.getInstance()

            db.collection("recorded_data")
                .document("${beacon.macAddress}_zone1_${name}")
                .set(mapOf("data" to recordedData))
                .addOnSuccessListener {
                    println("DATA INSERTED FOR ${beacon.macAddress}")
                }
                .addOnFailureListener {
                    println("Something went wrong while saving data in document. $it")
                }

            if (secondBeacon != null) {
                db.collection("recorded_data")
                    .document("${secondBeacon.macAddress}_zone2_${name}")
                    .set(mapOf("data" to recordedData2))
                    .addOnSuccessListener {
                        println("DATA INSERTED FOR ${secondBeacon.macAddress}")
                    }
                    .addOnFailureListener {
                        println("Something went wrong while saving data in document. $it")
                    }
            }

            if (thirdBeacon != null) {
                db.collection("recorded_data")
                    .document("${thirdBeacon.macAddress}_zone3_${name}")
                    .set(mapOf("data" to recordedData3))
                    .addOnSuccessListener {
                        println("DATA INSERTED FOR ${thirdBeacon.macAddress}")
                    }
                    .addOnFailureListener {
                        println("Something went wrong while saving data in document. $it")
                    }
            }

            if (fourthBeacon != null) {
                db.collection("recorded_data")
                    .document("${fourthBeacon.macAddress}_zone4_${name}")
                    .set(mapOf("data" to recordedData4))
                    .addOnSuccessListener {
                        println("DATA INSERTED FOR ${fourthBeacon.macAddress}")
                    }
                    .addOnFailureListener {
                        println("Something went wrong while saving data in document. $it")
                    }
            }
        }

        var recentTimestamp: Long = 0
        val cache = HashMap<String, Int>()
        var counter = 0

        override fun didFoundDevice(result: ScanResult) {
            val device = BeaconModel(result)

            if (recentTimestamp == 0L) recentTimestamp = Date().time

            if (!cache.containsKey(device.macAddress) && (device.macAddress == beacon.macAddress || device.macAddress == secondBeacon!!.macAddress || device.macAddress == thirdBeacon!!.macAddress || device.macAddress == fourthBeacon!!.macAddress)) {
                cache[device.macAddress] = device.rssi

                if (cache.size == 4) {
                    cache.forEach { (macAddress, rssi) ->
                        val map = HashMap<String, Any>()
                        map["rssi"] = rssi
                        map["timestamp"] = recentTimestamp

                        if (macAddress == beacon.macAddress) {
                            recordedData.add(map)
                            viewHolder.view.rssiInformation1.text = "$rssi"
                        }
                        if (macAddress == secondBeacon!!.macAddress) {
                            recordedData2.add(map)
                            viewHolder.view.rssiInformation2.text = "$rssi"
                        }
                        if (macAddress == thirdBeacon!!.macAddress) {
                            recordedData3.add(map)
                            viewHolder.view.rssiInformation3.text = "$rssi"
                        }
                        if (macAddress == fourthBeacon!!.macAddress) {
                            recordedData4.add(map)
                            viewHolder.view.rssiInformation4.text = "$rssi"
                        }

                        println("Recorded $recentTimestamp $rssi for $macAddress")
                    }

                    cache.clear()
                    recentTimestamp = 0L
                    counter += 1
                    viewHolder.view.counterTextView.text = "Counter: $counter"
                }
            }
        }

    }

    private class DeviceDetailViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    }

}