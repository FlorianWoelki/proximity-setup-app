package com.florianwoelki.ble_proximity_setup_app

import com.florianwoelki.ble_proximity_setup_app.api.Proxim
import kotlin.properties.Delegates

const val REQUEST_ACCESS_COARSE_LOCATION = 1
const val REQUEST_ACCESS_FINE_LOCATION = 11

var api: Proxim by Delegates.notNull()