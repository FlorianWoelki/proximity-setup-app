package com.florianwoelki.ble_proximity_setup_app.api.beacon.model

import java.io.Serializable

/**
 * This class represents the beacon packet model and it
 * gets the presented data from the packet.
 *
 * @property rawPacketData the raw hexadecimal packet data
 * @constructor Creates a beacon packet model with all its properties.
 */
class BeaconPacketModel(val rawPacketData: String) : Serializable {

    /**
     * Get the packet length in decimal.
     */
    val length = rawPacketData.substring(0, 2).toLong(16)

    /**
     * Get the flags of the packet in decimal.
     */
    val flags = rawPacketData.substring(2, 4).toLong(16)

    /**
     * Get the company in hexadecimal.
     */
    val company = rawPacketData.substring(4, 8).toUpperCase()

    /**
     * Get the manufacturer data in hexadecimal.
     */
    val manufacturerData = rawPacketData.substring(8)

}