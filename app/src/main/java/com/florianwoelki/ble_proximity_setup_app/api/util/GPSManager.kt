package com.florianwoelki.ble_proximity_setup_app.api.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

/**
 * This class represents a gps manager for getting the current device location.
 *
 * @property context the view context.
 * @property logger a proxim logger.
 */
class GPSManager(private val context: Context) {

    /**
     * Get the current device location.
     * This methods returns null, if the permissions are not set or the
     * gps provider is not enabled.
     *
     * @return an optional [Location]
     */
    fun getCurrentLocation(): Location? {
        // Check if permissions are set
        if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // Check if the gps provider is available.
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, GPSManagerListener())
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        }

        return null
    }

    /**
     * This private class represents a location listener.
     */
    private class GPSManagerListener : LocationListener {
        override fun onLocationChanged(location: Location?) {
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        }

        override fun onProviderEnabled(provider: String?) {
        }

        override fun onProviderDisabled(provider: String?) {
        }
    }

}
