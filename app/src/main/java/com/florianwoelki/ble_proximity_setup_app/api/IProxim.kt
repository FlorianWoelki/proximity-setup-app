package com.florianwoelki.ble_proximity_setup_app.api

import android.bluetooth.le.ScanResult

/**
 * This interface represents the API for Proxim.
 */
interface IProxim {

    /**
     * If a device was found this method gets called
     *
     * @param result the scan result which was found for the beacon
     */
    fun didFoundDevice(result: ScanResult)

}