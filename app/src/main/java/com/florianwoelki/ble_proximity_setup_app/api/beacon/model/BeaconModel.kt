package com.florianwoelki.ble_proximity_setup_app.api.beacon.model

import android.bluetooth.le.ScanResult
import com.florianwoelki.ble_proximity_setup_app.api.beacon.Distance
import java.io.Serializable

/**
 * This class represents the model object for the scanned
 * bluetooth low energy beacon.
 *
 * @param result the scanned result
 * @constructor Creates a serializable beacon data object model
 */
class BeaconModel(result: ScanResult) : Serializable {

    /**
     * Specifies the rssi. The signal strength of the beacon.
     */
    val rssi = result.rssi

    /**
     * Sets the distance with the rssi.
     */
    val distance = Distance(result.rssi)

    /**
     * Defines the tx power level.
     */
    val txPowerLevel = result.txPower

    /**
     * Sets the mac address.
     */
    val macAddress = result.device.address!!

    /**
     * Sets the name. In a lot of cases the name is null
     */
    val name: String? = result.device.name

    /**
     * Sets the advertising Sid.
     */
    val advertisingSid = result.advertisingSid

    /**
     * Creates a packet, which will decode the given hexadecimal string.
     */
    val packet = BeaconPacketModel(result.scanRecord?.bytes?.toHexString()!!)

    /**
     * This is a helper extension method for encoding a byte array to a hex string.
     */
    private fun ByteArray.toHexString() = joinToString("") {
        Integer.toUnsignedString(java.lang.Byte.toUnsignedInt(it), 16).padStart(2, '0')
    }

}