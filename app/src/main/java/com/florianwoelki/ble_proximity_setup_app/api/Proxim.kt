package com.florianwoelki.ble_proximity_setup_app.api

import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import com.florianwoelki.ble_proximity_setup_app.api.util.GPSManager

/**
 * This class represents the Proxim API with all its methods
 * and useful interfaces.
 *
 * @property context the view context
 * @property delegate the proxim interface
 * @constructor Creates the Proxim API
 */
class Proxim(private val context: Context, private val delegate: IProxim) {

    /**
     * Set up the bluetooth low energy scanning callback.
     */
    private val bleScanner = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            if (result?.device?.address != null) {
                // logger.info("Scan Result: (rssi: ${result.rssi}, mac address: ${result.device?.address}, name: ${result.device?.name})")
                delegate.didFoundDevice(result)
            }
        }
    }

    /**
     * This sets up the bluetooth low energy scanner.
     */
    private val bluetoothLeScanner: BluetoothLeScanner
        get() {
            val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothAdapter = bluetoothManager.adapter
            return bluetoothAdapter.bluetoothLeScanner
        }

    /**
     * This gps manager, allows you to get the current location of the device.
     */
    val gpsManager = GPSManager(context)

    /**
     * Start scanning ble devices in the near of the owner device.
     * With the parameter [scanMode] you can specify the scan mode for
     * the specific bluetooth low energy scan.
     */
    fun startScanning(scanMode: Int = ScanSettings.SCAN_MODE_LOW_LATENCY) {
        // Build scan settings
        val builderScanSettings = ScanSettings.Builder()
        builderScanSettings.setScanMode(scanMode)

        // Start scanning
        bluetoothLeScanner.startScan(null, builderScanSettings.build(), bleScanner)

        /*Timer("BLE Scanner Stop", false).schedule(20 * 1000) {
            stopScanning()
            Timer("BLE Scanner Start", false).schedule(1000 * 3) {
                startScanning()
            }
        }*/
    }

    /**
     * Stop scanning of Bluetooth low energy devices.
     */
    fun stopScanning() {
        bluetoothLeScanner.stopScan(bleScanner)
    }

}