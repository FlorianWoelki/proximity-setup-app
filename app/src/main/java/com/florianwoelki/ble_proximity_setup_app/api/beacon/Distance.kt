package com.florianwoelki.ble_proximity_setup_app.api.beacon

import java.io.Serializable

/**
 * This class represents a distance in words.
 *
 * @property rssi the signal strength to the owner device.
 * @constructor Creates a serializable Distance object.
 */
class Distance(val rssi: Int) : Serializable {

    /**
     * Gets the type of distance for the given [rssi].
     */
    fun getType(): DistanceType? {
        return DistanceType.getTypeByRssi(rssi)
    }

}