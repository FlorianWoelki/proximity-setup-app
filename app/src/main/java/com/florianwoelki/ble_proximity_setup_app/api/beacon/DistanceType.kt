package com.florianwoelki.ble_proximity_setup_app.api.beacon

/**
 * This enum class represents the Distance Type in clear words.
 *
 * @property from where the distance type will start to get recognized.
 * @property to where the distance type will end to get recognized.
 * @constructor Creates a distance type with from and to values.
 */
enum class DistanceType(val from: Int, val to: Int) {

    /**
     * The bluetooth low energy beacon is immediate away.
     */
    IMMEDIATE(0, -50),

    /**
     * The bluetooth low energy beacon is near.
     */
    NEAR(-51, -75),

    /**
     * The bluetooth low energy beacon is far away.
     */
    FAR(-75, -100);

    companion object {
        /**
         * Gets the type by the rssi signal strength
         *
         * @param rssi the bluetooth low energy beacon signal strength as an int.
         * @return optional distance type.
         */
        fun getTypeByRssi(rssi: Int): DistanceType? {
            values().forEach {
                if (rssi < it.from && rssi > it.to) {
                    return it
                }
            }
            return null
        }
    }

}