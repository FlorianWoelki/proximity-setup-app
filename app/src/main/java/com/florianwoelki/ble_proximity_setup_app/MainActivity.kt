package com.florianwoelki.ble_proximity_setup_app

import android.Manifest
import android.app.Activity
import android.bluetooth.le.ScanResult
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.florianwoelki.ble_proximity_setup_app.api.IProxim
import com.florianwoelki.ble_proximity_setup_app.api.Proxim
import com.florianwoelki.ble_proximity_setup_app.api.beacon.model.BeaconModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), IProxim {

    private var adapter: MainAdapter by Delegates.notNull()
    private val checkedDevices = LinkedHashMap<String, BeaconModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkFineLocationPermission()
        checkCoarseLocationPermission()

        api = Proxim(this, this)
        api.gpsManager.getCurrentLocation()

        val btn = findViewById<FloatingActionButton>(R.id.goToRecordScreenBtn)
        btn.isEnabled = false
        btn.setOnClickListener {
            val intent = Intent(this, BLEDeviceDetailActivity::class.java)
            // intent.putExtra("SELECTED_DEVICES", checkedDevices.values.toList().toTypedArray())
            intent.putExtra(MainViewHolder.DEVICE_TITLE_KEY, "Record Devices")
            intent.putExtra(MainViewHolder.BEACON_INFO_KEY, checkedDevices.values.toList()[0])
            intent.putExtra(MainViewHolder.SECOND_BEACON_INFO_KEY, checkedDevices.values.toList()[1])
            intent.putExtra(MainViewHolder.THIRD_BEACON_INFO_KEY, checkedDevices.values.toList()[2])
            intent.putExtra(MainViewHolder.FOURTH_BEACON_INFO_KEY, checkedDevices.values.toList()[3])

            api.stopScanning()
            startActivity(intent)
        }

        adapter = MainAdapter(this, api, checkedDevices, btn)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    fun onRefreshAction(mi: MenuItem) {
        adapter.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()

        api.startScanning()
    }

    override fun didFoundDevice(result: ScanResult) {
        val device = BeaconModel(result)
        if (!adapter.devices.containsKey(result.device.address)) {
            adapter.devices[result.device.address] = device
            adapter.notifyDataSetChanged()
        } else {
            val rssi = adapter.devices[result.device.address]?.distance?.rssi
            if (rssi != null && rssi != device.distance.rssi) {
                adapter.devices[result.device.address] = device
            }
        }
    }

    private fun checkCoarseLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_ACCESS_COARSE_LOCATION)
            return false
        }
        return true
    }

    private fun checkFineLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_ACCESS_FINE_LOCATION)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_ACCESS_COARSE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Access coarse location enabled. You can start scanning ble devices", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Coarse location not enabled! Cannot scan ble devices", Toast.LENGTH_SHORT).show()
                }
            }
            REQUEST_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Access fine location enabled...", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Coarse location not enabled!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        val tflite = Interpreter(loadModelFile(this))
        val inputBuffer = floatArrayOf(-20f, -30f, -80f, -70f)
        val outputArray = arrayOf(FloatArray(2))
        tflite.run(inputBuffer, outputArray)
        println(outputArray[0].size)
        println(outputArray[0][0])
        println(outputArray[0][1])
    }

    private fun loadModelFile(activity: Activity): MappedByteBuffer {
        val modelPath = "converted_model.tflite"

        val fileDescriptor = activity.assets.openFd(modelPath)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength

        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }
}
