package com.florianwoelki.ble_proximity_setup_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.florianwoelki.ble_proximity_setup_app.api.beacon.model.BeaconModel
import kotlinx.android.synthetic.main.activity_blerecord.*
import kotlin.properties.Delegates

class BLERecordActivity : AppCompatActivity() {

    private var adapter: BLERecordAdapter by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blerecord)
        supportActionBar!!.title = "Record Devices"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val checkedDevices = intent.getSerializableExtra("SELECTED_DEVICES") as Array<BeaconModel>

        adapter = BLERecordAdapter(checkedDevices.toList())

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }
}
