package com.florianwoelki.ble_proximity_setup_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.florianwoelki.ble_proximity_setup_app.api.beacon.model.BeaconModel
import kotlinx.android.synthetic.main.device_record_row.view.*

class BLERecordViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
}

class BLERecordAdapter(val checkedDevices: List<BeaconModel>) : RecyclerView.Adapter<BLERecordViewHolder>() {

    override fun getItemCount(): Int {
        return checkedDevices.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BLERecordViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.device_record_row, parent, false)
        return BLERecordViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: BLERecordViewHolder, position: Int) {
        holder.view.currentRssiTextView.text = "${checkedDevices[position].rssi}"
        holder.view.beaconTextView.text = checkedDevices[position].macAddress
    }

}