package com.florianwoelki.ble_proximity_setup_app

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.florianwoelki.ble_proximity_setup_app.api.Proxim
import com.florianwoelki.ble_proximity_setup_app.api.beacon.model.BeaconModel
import com.florianwoelki.ble_proximity_setup_app.api.util.GPSManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.device_row.view.*

class MainViewHolder(val view: View, var beacon: BeaconModel? = null, var secondBeacon: BeaconModel? = null, var thirdBeacon: BeaconModel? = null, var fourthBeacon: BeaconModel? = null, var gpsManager: GPSManager? = null) : RecyclerView.ViewHolder(view) {

    companion object {
        const val GPS_LATITUDE = "GPS_LATITUDE"
        const val GPS_LONGITUDE = "GPS_LONGITUDE"
        const val BEACON_INFO_KEY = "BEACON_INFO"
        const val SECOND_BEACON_INFO_KEY = "SECOND_BEACON_INFO"
        const val THIRD_BEACON_INFO_KEY = "THIRD_BEACON_INFO"
        const val FOURTH_BEACON_INFO_KEY = "FOURTH_BEACON_INFO"
        const val DEVICE_TITLE_KEY = "DEVICE_TITLE"
    }

    /*init {
        view.setOnClickListener {
            val intent = Intent(view.context, BLEDeviceDetailActivity::class.java)

            intent.putExtra(GPS_LATITUDE, gpsManager?.getCurrentLocation()?.latitude)
            intent.putExtra(GPS_LONGITUDE, gpsManager?.getCurrentLocation()?.longitude)
            intent.putExtra(BEACON_INFO_KEY, beacon)
            intent.putExtra(DEVICE_TITLE_KEY,
                if (beacon?.macAddress!!.startsWith("C4"))
                    "All 4 Beacons"
                else beacon?.macAddress
            )

            if (secondBeacon != null) intent.putExtra(SECOND_BEACON_INFO_KEY, secondBeacon)
            if (thirdBeacon != null) intent.putExtra(THIRD_BEACON_INFO_KEY, thirdBeacon)
            if (fourthBeacon != null) intent.putExtra(FOURTH_BEACON_INFO_KEY, fourthBeacon)

            api.stopScanning()
            view.context.startActivity(intent)
        }
    }*/

}

class MainAdapter(val context: Context, val api: Proxim, val checkedDevices: LinkedHashMap<String, BeaconModel>, val fab: FloatingActionButton) : RecyclerView.Adapter<MainViewHolder>() {

    val devices = LinkedHashMap<String, BeaconModel>()

    override fun getItemCount(): Int {
        return devices.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.device_row, parent, false)
        return MainViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val devicesList = devices.values.toList()
        holder.view.beaconMacAdress.text = devicesList[position].macAddress
        holder.view.rssiTextView.text = "${devicesList[position].distance.rssi}"

        val device = devicesList[position]
        if (checkedDevices.containsKey(device.macAddress)) {
            holder.view.beaconMacAdress.isChecked = true
        }

        holder.view.beaconMacAdress.setOnClickListener {
            if (holder.view.beaconMacAdress.isChecked) checkedDevices[device.macAddress] = devices[device.macAddress]!!
            else checkedDevices.remove(device.macAddress)

            fab.isEnabled = checkedDevices.size > 0
            if (checkedDevices.size > 0) {
                fab.backgroundTintList = ColorStateList.valueOf(context.resources.getColor(R.color.colorAccent))
            } else {
                fab.backgroundTintList = ColorStateList.valueOf(context.resources.getColor(android.R.color.darker_gray))
            }
        }

        holder.beacon = devicesList[position]

        holder.gpsManager = api.gpsManager
    }

}